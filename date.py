import urllib.request
import os

try:
    with urllib.request.urlopen("http://just-the-time.appspot.com/?f=%Y%m%d%H%M%S") as url:
        date = str(url.read())
        date = date[2:-1]
    year = date[:4]
    month = date[4:6]
    day = date[6:8]
    hour = date[8:10]
    minutes = date[10:12]
    seconds = date[12:14]
    cmd = "sudo date -s \"%s-%s-%s %s:%s:%s UTC\"\n"%(year,month,day,hour,minutes,seconds)
    os.system(cmd)
except:
    pass
    
