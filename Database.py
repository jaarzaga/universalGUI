import sys # always seem to need this
import os

def add_unit(passed, file):
    _file = open (file, "r+")
    logfile = _file.read()
    if (len(logfile)>10):
        if (passed):
            current = int(logfile[5:11])
            new = current + 1;
            new_line = logfile[0:5] + str(new).zfill(6) + logfile[11:]
        else:        
            current = int(logfile[17:23])
            new = current + 1;
            new_line = logfile[0:17] + str(new).zfill(6) + logfile[23:]
    else:
        if(passed):
            new_line ="Pass 000001 Fail 000000\n"
        else:
            new_line ="Pass 000000 Fail 000001\n"
    _file.close()            
    logfile = open (file, "w+")
    logfile.write(new_line)
    logfile.close()
    return 

def clear_file(file):
    logfile = open (file, "w")
    logfile.write("")
    logfile.close()
    

def verify_file(file):    
    if os.path.exists(file):
        pass
    else:
        logfile = open (file, "w+")  
        logfile.close()
    return

def get_units(file):
    _file = open (file, "r+")
    logfile = _file.read()
    if (len(logfile)>10):
        good = int(logfile[5:11])        
        bad = int(logfile[17:23])        
    else:
        good = 0
        bad = 0
    _file.close()                
    return good, bad

def get_filename():    
    curr_path = os.path.realpath(__file__)
    path = curr_path[:-7] + "/"
    file_name = path + "counter.txt"    
    return file_name
