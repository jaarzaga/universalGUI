#### General files
import sys 
import os
import PyQt5 # This gets the Qt stuff
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import configparser
import time
import platform
import threading
from Database import *

################### GUI files  ####################################################
import MainWindow #Main window of GUI
import frmLock #Password window to enable restricted settings

################### To determine if running on PC (for deubg and development) or RPI for productoin 
if platform.system() != "Windows":
    from GPIO_functions import *
    RUNNING_WINDOWS = 0
    FULLSCREEN = 1
    ONSCREEN_LOG = 0
else:
    from msvcrt import getch
    RUNNING_WINDOWS = 1
    FULLSCREEN = 0
    ONSCREEN_LOG = 0
##################### Variable for version control ##############################################
VERSION = "v1.0"
NAME = "Universal - Vision Sistem GUI"    
##################### Variables for debug purposes, input simulations ###########################
SENSOR1_ACTIVE = "1"
SENSOR1_INACTIVE = "2"
SENSOR2_ACTIVE = "3"
SENSOR2_INACTIVE = "4"
CAMSTATUS_OK = "5"
CAMSTATUS_ERROR = "6"
TRIGGERIN_ACTIVE = "7"
TRIGGERIN_INACTIVE = "8"
CAM_PASS = "9"
CAM_FAIL = "0"
##################### Hardware PINOUT assignment ################################################
if RUNNING_WINDOWS:
    GPIO_SENSOR1 = 0
    GPIO_SENSOR2 = 0
    GPIO_CAMSTATUS = 0
    GPIO_TRIGGERIN = 0
    GPIO_RESULT = 0

    GPIO_PISTON = 0
    GPIO_BIT0 = 0
    GPIO_BIT1 = 0
    GPIO_BIT2 = 0
    GPIO_BIT3 = 0
    GPIO_BIT4 = 0
    GPIO_TRIGGEROUT = 0
    GPIO_LEDGREEN = 0
    GPIO_LEDRED = 0

    HIGH = 1
    LOW = 0

    HIGH_NPN = 1
    LOW_NPN = 0
##################### Internal Global constants ################################################
SYNC_TIME_IN = 0.0001
SYNC_TIME_OUT = 0.0001

MODE_NONE = 0
MODE_RUNNING = 1
MODE_SELECT = 2

BIT0 = 0x01
BIT1 = 0x02
BIT2 = 0x04
BIT3 = 0x08
BIT4 = 0x10

ACTIVE = True
INACTIVE = False

STATE_HOMEPOS = 0
STATE_MOVING = 1
STATE_ENDPOS = 2

EDGE_NONE = 0
EDGE_RISING = 1
EDGE_FALLING = 2

PASSED = True
FAILED = False

DELAY = 25
#################################################################################################

 
class MainWindow(QMainWindow, MainWindow.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self) 
        self.btnNext.clicked.connect(self.pressedNext)
        self.btnPrevious.clicked.connect(self.pressedPrevious)
        self.btnSelect.clicked.connect(self.pressedSelect)
        self.btnReset.clicked.connect(self.pressedReset)
        self.dlgLock = frmLock()        
        self.show()

    def pressedSelect(self):
        secure = 0
        if (Config["Settings"]["Secure mode"] == "True"):
            GUI.password = ""
            if self.dlgLock.exec_():
                if verify_password(GUI.password): secure = 1
        else:
            secure = 1        
        if(secure):
            if GUI.mode == MODE_RUNNING:
                self.btnNext.setEnabled(True)
                self.btnPrevious.setEnabled(True)
                GUI.mode = MODE_SELECT
                self.btnSelect.setText("Select\nModel")
            elif GUI.mode == MODE_SELECT:
                self.btnNext.setEnabled(False)
                self.btnPrevious.setEnabled(False)                
                GUI.mode = MODE_RUNNING
                self.btnSelect.setText("Change\nModel")
            else:
                self.btnNext.setEnabled(True)
                self.btnPrevious.setEnabled(True)
                GUI.mode = MODE_SELECT
                self.btnSelect.setText("Select\nModel")
        else:
            pass
         
    def pressedReset(self):
        secure = 0
        if (Config["Settings"]["Secure mode"] == "True"):
            GUI.password = ""
            if self.dlgLock.exec_():
                if verify_password(GUI.password): secure = 1
        else:
            secure = 1        
        if(secure):
            _file = get_filename()
            verify_file(_file)
            clear_file(_file)            
        else:
            pass

    def keyPressEvent(self, event):
        GUI.keypressed = event.text()
	
    def pressedNext(self):
        _current = GUI.program.selected
        if (_current == MODELS_NO):
            _current = 0
        else:
            _current = _current + 1
        _picture = "resources/" + Config["Pictures"][str(_current)]
        printl(_picture)
        self.lblImage.setPixmap(PyQt5.QtGui.QPixmap(_picture))
        self.lblModel.setText(Config["Models"][str(_current)])
        GUI.program.selected = _current
        GUI.program.changeflag = True
        
    def pressedPrevious(self):
        _current = GUI.program.selected
        if (_current == 0):
            _current = MODELS_NO
        else:
            _current = _current - 1
        _picture = "resources/" + Config["Pictures"][str(_current)]
        printl(_picture)
        self.lblImage.setPixmap(PyQt5.QtGui.QPixmap(_picture))
        self.lblModel.setText(Config["Models"][str(_current)])
        GUI.program.selected = _current
        GUI.program.changeflag = True
    

    def closeEvent(self, event):
        printl("Program Closed")
        GUI.running = False
        for t in threads:
            t.join()
        printl("End of threads")
        
    def showEvent(self, event):
        _current = GUI.program.selected
        _picture = "resources/" + Config["Pictures"][str(_current)]
        self.lblImage.setPixmap(PyQt5.QtGui.QPixmap(_picture))
        self.lblResult.setEnabled(False)
        self.lblTitle.setText(NAME+" "+VERSION)

def verify_password(password):
    if Config['Settings']['Password'] == password: return 1
    else: return 0
    
        
class frmLock(QDialog, frmLock.Ui_frmLock):
    def __init__(self):
        super().__init__()
        self.setupUi(self)        
        self.btnNum0.clicked.connect(self.pressedNum0)
        self.btnNum1.clicked.connect(self.pressedNum1)
        self.btnNum2.clicked.connect(self.pressedNum2)
        self.btnNum3.clicked.connect(self.pressedNum3)
        self.btnNum4.clicked.connect(self.pressedNum4)
        self.btnNum5.clicked.connect(self.pressedNum5)
        self.btnNum6.clicked.connect(self.pressedNum6)
        self.btnNum7.clicked.connect(self.pressedNum7)
        self.btnNum8.clicked.connect(self.pressedNum8)
        self.btnNum9.clicked.connect(self.pressedNum9)
        self.btnAccept.clicked.connect(self.accept)
        self.btnReject.clicked.connect(self.reject)

    def changeEvent(self, event):
        if(self.isMinimized()): self.showNormal()
        if(self.isMaximized()): self.showNormal()
        
    def showEvent(self, event):
        self.lblPassword.setText("")        
        self.btnAccept.setEnabled(True)
        self.btnReject.setEnabled(True)
        self.btnNum0.setEnabled(True)
        self.btnNum1.setEnabled(True)
        self.btnNum2.setEnabled(True)
        self.btnNum3.setEnabled(True)
        self.btnNum4.setEnabled(True)
        self.btnNum5.setEnabled(True)
        self.btnNum6.setEnabled(True)
        self.btnNum7.setEnabled(True)
        self.btnNum8.setEnabled(True)
        self.btnNum9.setEnabled(True)
    
    def pressedNum0(self):self.write_digit("0")
    def pressedNum1(self):self.write_digit("1")
    def pressedNum2(self):self.write_digit("2")
    def pressedNum3(self):self.write_digit("3")
    def pressedNum4(self):self.write_digit("4")
    def pressedNum5(self):self.write_digit("5")
    def pressedNum6(self):self.write_digit("6")
    def pressedNum7(self):self.write_digit("7")
    def pressedNum8(self):self.write_digit("8")
    def pressedNum9(self):self.write_digit("9")
        
    def write_digit(self,digit):
        if len(self.lblPassword.text())<4:
            GUI.password = GUI.password + digit
            self.lblPassword.setText(self.lblPassword.text() + "*")
        if len(self.lblPassword.text())==4:
            self.btnNum0.setEnabled(False)
            self.btnNum1.setEnabled(False)
            self.btnNum2.setEnabled(False)
            self.btnNum3.setEnabled(False)
            self.btnNum4.setEnabled(False)
            self.btnNum5.setEnabled(False)
            self.btnNum6.setEnabled(False)
            self.btnNum7.setEnabled(False)
            self.btnNum8.setEnabled(False)
            self.btnNum9.setEnabled(False)
            
            
        
def read_config_file():
    global Config
    curr_path = os.path.realpath(__file__)
    path = curr_path[:-7] + "resources/"
    file = "config.ini"
    Config = configparser.ConfigParser()
    Config.read(path+file)
    printl("Config file loaded")

def write_config_file():
    curr_path = os.path.realpath(__file__)
    path = curr_path[:-7] + "resources/"
    file = "config.ini"
    with open(path + file, 'w') as configfile:
        Config.write(configfile)
        configfile.close()
    printl("Config file updated")

################## Main Variable of program #########################################
class GUIStatus:
    class dependentOut:
        changeflag = False # Boolean to indicate if something changed
        selected = 0 # Option selected, is the number of program could be 0 to 5
        toprocess = False  # Boolean to indicate if something is pending to pocess  

    class Outputs:
        class Output:
            gui_active = False
            gui_inactive = True
            gui_object = None
            pi_active = HIGH
            pi_inactive = LOW
            pi_object = None

        ledgreen = Output()
        ledred = Output()
        piston = Output()
        triggerout = Output()        
        
    class Inputs:
        class Input:
            state_current = INACTIVE
            state_previous = INACTIVE
            changeflag = False
            edge = EDGE_NONE
            toprocess = False
        
        sensor1 = Input()        
        sensor2 = Input()
        trigger = Input()
        result = Input()
        status = Input()
    
    keypressed = ""
    password = ""
    running = True
    inputs = Inputs()
    outputs = Outputs()
    mode = MODE_NONE #Mode of program, could be selecting or runing
    pi = 0
    
    program = dependentOut()    
#####################################################################################
def Outputs():
    def setOut(target,state):
        if state == ACTIVE:
            target.gui_object.setEnabled(target.gui_active)
        elif state == INACTIVE:
            target.gui_object.setEnabled(target.gui_inactive)
        if not RUNNING_WINDOWS:
            if state == ACTIVE:
                setOutput(GUI.pi, target.pi_object,target.pi_active)                
            elif state == INACTIVE:
                setOutput(GUI.pi, target.pi_object,target.pi_inactive)

    def setProgram(_program):
        if _program & BIT0:
            form.lblBit0.setEnabled(False)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT0,HIGH_NPN)
        else:
            form.lblBit0.setEnabled(True)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT0,LOW_NPN)
        if _program & BIT1:
            form.lblBit1.setEnabled(False)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT1,HIGH_NPN)
        else:
            form.lblBit1.setEnabled(True)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT1,LOW_NPN)
        if _program & BIT2:
            form.lblBit2.setEnabled(False)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT2,HIGH_NPN)
        else:
            form.lblBit2.setEnabled(True)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT2,LOW_NPN)
        if _program & BIT3:
            form.lblBit3.setEnabled(False)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT3,HIGH_NPN)
        else:
            form.lblBit3.setEnabled(True)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT3,LOW_NPN)
        if _program & BIT4:
            form.lblBit4.setEnabled(False)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT4,HIGH_NPN)
        else:
            form.lblBit4.setEnabled(True)
            if not RUNNING_WINDOWS:
                setOutput(GUI.pi, GPIO_BIT4,LOW_NPN)
        
    def processProgram():
        _program = int(Config["Programs"][str(GUI.program.selected)],16)
        setProgram(_program)
        
    def processTrigger():
        _mode = GUI.mode
        _home = GUI.inputs.sensor1.state_current
        _busy = GUI.inputs.status.state_current        
        _edge = GUI.inputs.trigger.edge
        _triggerout = GUI.outputs.triggerout
        if (_home == ACTIVE and _busy == INACTIVE and  _mode == MODE_RUNNING):            
            if (_edge == EDGE_RISING):
                setOut(_triggerout, ACTIVE)
            if (_edge == EDGE_FALLING):
                setOut(_triggerout, INACTIVE)
            else:
                pass
            
    def processStatus():
        _mode = GUI.mode
        _home = GUI.inputs.sensor1.state_current
        _edge = GUI.inputs.status.edge
        _result = GUI.inputs.result.state_current
        _red = GUI.outputs.ledred
        _green = GUI.outputs.ledgreen
        _piston = GUI.outputs.piston
        _triggerout = GUI.outputs.triggerout
        printl("processing") 
        if (_home == ACTIVE and _mode == MODE_RUNNING):
            if (_edge == EDGE_FALLING):
                setOut(_triggerout, INACTIVE)
                if(_result ==  ACTIVE):                    
                    setOut(_red, INACTIVE)
                    setOut(_green, ACTIVE)
                    setOut(_piston, INACTIVE)
                    form.lblResult.setEnabled(False)
                elif(_result ==  INACTIVE):
                    setOut(_red, ACTIVE)
                    setOut(_green, INACTIVE)
                    setOut(_piston, ACTIVE)
                    form.lblResult.setEnabled(True)
                    _file = get_filename()
                    verify_file(_file)
                    add_unit(FAILED, _file)
                    _file = _file[:-6] + ".txt"
                    verify_file(_file)
                    add_unit(FAILED, _file)
                else:
                    pass
            else:
                pass

    def processHome():
        _mode = GUI.mode
        _edge = GUI.inputs.sensor1.edge
        _result = GUI.inputs.result.state_current
        _red = GUI.outputs.ledred
        _green = GUI.outputs.ledgreen
        _piston = GUI.outputs.piston
        if (_mode == MODE_RUNNING):
            if (_edge == EDGE_RISING):
                setOut(_red, INACTIVE)
                setOut(_green, INACTIVE)
                setOut(_piston, ACTIVE)
            else:
                pass

    def processEnd():
        _mode = GUI.mode
        _edge = GUI.inputs.sensor2.edge
        _result = GUI.inputs.result.state_current
        if (_mode == MODE_RUNNING):
            if (_edge == EDGE_RISING):
                pass
            else:
                pass
        
    threadLock = threading.Lock()
    _delay=0
    _increase=0
    while GUI.running:
        threadLock.acquire()
        try:
            _file = get_filename()
            verify_file(_file)
            _good, _bad = get_units(_file)              
            form.lblTotaltested.display(_good)
            form.lblTotalbad.display(_bad)
            
            if(GUI.mode != MODE_RUNNING):
                setOut(GUI.outputs.piston, ACTIVE)
            if(GUI.program.changeflag):
                processProgram()        
                GUI.program.changeflag = False
            if(GUI.program.selected):                
                if(GUI.inputs.trigger.changeflag):
                    processTrigger()        
                    GUI.inputs.trigger.changeflag = False
                if(GUI.inputs.status.changeflag):
                    processStatus()
                    GUI.inputs.status.changeflag = False
                if(GUI.inputs.sensor1.changeflag):
                    processHome()
                    GUI.inputs.sensor1.changeflag = False
                    _delay=0;
                    if (GUI.inputs.sensor1.edge==EDGE_RISING):
                        _increase = 1;
                    else:
                        _increase = 0;
                _delay=_delay + _increase;
                if(_delay >= DELAY):
                    _delay=0
                    _increase=0
                    add_unit(PASSED, _file)
                    _file = _file[:-6] + ".txt"
                    verify_file(_file)
                    add_unit(PASSED, _file)                
                if(GUI.inputs.sensor2.changeflag):
                    processEnd()
                    GUI.inputs.sensor2.changeflag = False
            app.processEvents()
            time.sleep(0.05)
        finally:            
            threadLock.release()

class Input(threading.Thread):    
    def __init__(self, active_value, inactive_value, gui_object, global_variable, channel, pi = 0): 
        threading.Thread.__init__(self) 
        self.active = active_value
        self.inactive = inactive_value
        self.gui = gui_object
        self.channel = channel
        self.variable = global_variable

    def activate(self):
        self.gui.setEnabled(False)
        self.variable.state_previous = self.variable.state_current
        self.variable.state_current = ACTIVE
        if(self.variable.state_previous != self.variable.state_current):
            self.variable.changeflag = True
            self.variable.edge = EDGE_RISING
        
    def deactivate(self):
        self.gui.setEnabled(True)
        self.variable.state_previous = self.variable.state_current
        self.variable.state_current = INACTIVE
        if(self.variable.state_previous != self.variable.state_current):
            printl("to process ", self.active) 
            self.variable.changeflag = True
            self.variable.edge = EDGE_FALLING
        
    def run(self):
        threadLock = threading.Lock()    
        self.deactivate()
        while GUI.running:
            threadLock.acquire()
            try:
                _start = time.time()
                if RUNNING_WINDOWS:
                    if GUI.keypressed == self.active: self.activate()
                    elif GUI.keypressed == self.inactive: self.deactivate()
                else:
                    if getInput(GUI.pi, self.channel): self.deactivate()
                    else: self.activate()                
            finally:
                threadLock.release()
                time.sleep(SYNC_TIME_IN)            

def printl(*arg):
    if ONSCREEN_LOG:
        a=len(arg)
        s = ""
        for i in range(0,a):
            s=s+str(arg[i])
        print(s)    

  
def main():
    global GUI
    global app
    global pi
    global form
    global threads
    global MODELS_NO
    
    read_config_file()  #Read ini file and load configurations
    GUI = GUIStatus()   #Main variable of the code, contains status of execution    

    #Read qty of models from config file
    MODELS_NO = int(Config["Settings"]["models"])
    threads = []
    
    if not RUNNING_WINDOWS:
        GUI.pi = initialize_gpios()
        
    printl("Program started")
    
    # a new app instance
    app = QApplication(sys.argv)
    form = MainWindow()    

    GUI.outputs.ledgreen.gui_object = form.lblGreenLight    
    GUI.outputs.ledgreen.pi_object = GPIO_LEDGREEN
    GUI.outputs.ledred.gui_object = form.lblRedLight    
    GUI.outputs.ledred.pi_object = GPIO_LEDRED
    GUI.outputs.piston.gui_object = form.lblPiston
    GUI.outputs.piston.pi_object = GPIO_PISTON
    GUI.outputs.triggerout.gui_object = form.lblTriggerOut
    GUI.outputs.triggerout.pi_active = HIGH_NPN
    GUI.outputs.triggerout.pi_inactive = LOW_NPN
    GUI.outputs.triggerout.pi_object = GPIO_TRIGGEROUT
    
    thread1 = Input(SENSOR1_ACTIVE,SENSOR1_INACTIVE,form.lblSensor1,GUI.inputs.sensor1, GPIO_SENSOR1, GUI.pi)
    thread2 = Input(SENSOR2_ACTIVE,SENSOR2_INACTIVE,form.lblSensor2,GUI.inputs.sensor2, GPIO_SENSOR2, GUI.pi)
    thread3 = Input(CAMSTATUS_OK,CAMSTATUS_ERROR,form.lblCamBusy,GUI.inputs.status, GPIO_CAMSTATUS, GUI.pi)
    thread4 = Input(TRIGGERIN_ACTIVE,TRIGGERIN_INACTIVE,form.lblTriggerIn,GUI.inputs.trigger, GPIO_TRIGGERIN, GUI.pi)
    thread5 = Input(CAM_PASS,CAM_FAIL,form.lblCamResult,GUI.inputs.result, GPIO_RESULT, GUI.pi)
    thread6 = threading.Thread(target = Outputs)        
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread6.start()        
    threads.append(thread1)
    threads.append(thread2)
    threads.append(thread3)
    threads.append(thread4)
    threads.append(thread5)
    threads.append(thread6)    
    
    if FULLSCREEN: form.showFullScreen()
    else: form.show()
    
    sys.exit(app.exec_())    
    exit()
 
# Main program start here
if __name__ == "__main__":
    main()
