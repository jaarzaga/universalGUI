import pigpio

##################### Hardware PINOUT assignment ################################################
GPIO_SENSOR1 = 14
GPIO_SENSOR2 = 15
GPIO_CAMSTATUS = 20
GPIO_TRIGGERIN = 18
GPIO_RESULT = 21

GPIO_PISTON = 3
GPIO_BIT0 = 26
GPIO_BIT1 = 19
GPIO_BIT2 = 13
GPIO_BIT3 = 6
GPIO_BIT4 = 5
GPIO_TRIGGEROUT = 12
GPIO_LEDGREEN = 10
GPIO_LEDRED = 22

HIGH = 1
LOW = 0

HIGH_NPN = 0
LOW_NPN = 1

def initialize_gpios():
     pi = pigpio.pi()
     if not pi.connected:
          exit()
     pi.set_mode(GPIO_SENSOR1,pigpio.INPUT)
     pi.set_mode(GPIO_SENSOR2,pigpio.INPUT)
     pi.set_mode(GPIO_CAMSTATUS,pigpio.INPUT)
     pi.set_mode(GPIO_TRIGGERIN,pigpio.INPUT)
     pi.set_mode(GPIO_RESULT,pigpio.INPUT)

     pi.set_pull_up_down(GPIO_SENSOR1, pigpio.PUD_UP)
     pi.set_pull_up_down(GPIO_SENSOR2, pigpio.PUD_UP)
     pi.set_pull_up_down(GPIO_CAMSTATUS, pigpio.PUD_UP)
     pi.set_pull_up_down(GPIO_TRIGGERIN, pigpio.PUD_UP)
     pi.set_pull_up_down(GPIO_RESULT, pigpio.PUD_UP)
     
     pi.set_mode(GPIO_PISTON,pigpio.OUTPUT)
     pi.set_mode(GPIO_BIT0,pigpio.OUTPUT)
     pi.set_mode(GPIO_BIT1,pigpio.OUTPUT)
     pi.set_mode(GPIO_BIT2,pigpio.OUTPUT)
     pi.set_mode(GPIO_BIT3,pigpio.OUTPUT)
     pi.set_mode(GPIO_BIT4,pigpio.OUTPUT)     
     pi.set_mode(GPIO_TRIGGEROUT,pigpio.OUTPUT)
     pi.set_mode(GPIO_LEDGREEN,pigpio.OUTPUT)
     pi.set_mode(GPIO_LEDRED,pigpio.OUTPUT)     
          
     pi.write(GPIO_PISTON,0)
     pi.write(GPIO_BIT0,LOW_NPN)
     pi.write(GPIO_BIT1,LOW_NPN)
     pi.write(GPIO_BIT2,LOW_NPN)
     pi.write(GPIO_BIT3,LOW_NPN)
     pi.write(GPIO_BIT4,LOW_NPN)     
     pi.write(GPIO_TRIGGEROUT,0)
     pi.write(GPIO_LEDGREEN,0)
     pi.write(GPIO_LEDRED,0)
     
     return pi

def setOutput(pi,gpio,status):
     pi.write(gpio,status)
     
def getInput(pi,gpio):
     return pi.read(gpio)
     
