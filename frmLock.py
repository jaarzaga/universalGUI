# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'frmLock.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_frmLock(object):
    def setupUi(self, frmLock):
        frmLock.setObjectName("frmLock")
        frmLock.resize(301, 372)
        frmLock.setMinimumSize(QtCore.QSize(215, 353))
        frmLock.setMaximumSize(QtCore.QSize(800, 480))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        frmLock.setPalette(palette)
        self.gridLayoutWidget = QtWidgets.QWidget(frmLock)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 281, 341))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setHorizontalSpacing(11)
        self.gridLayout.setVerticalSpacing(8)
        self.gridLayout.setObjectName("gridLayout")
        self.btnNum2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum2.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum2.setFont(font)
        self.btnNum2.setObjectName("btnNum2")
        self.gridLayout.addWidget(self.btnNum2, 1, 1, 1, 1)
        self.btnNum9 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum9.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum9.setFont(font)
        self.btnNum9.setObjectName("btnNum9")
        self.gridLayout.addWidget(self.btnNum9, 4, 2, 1, 1)
        self.btnNum0 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum0.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum0.setFont(font)
        self.btnNum0.setObjectName("btnNum0")
        self.gridLayout.addWidget(self.btnNum0, 5, 0, 1, 1)
        self.btnNum3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum3.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum3.setFont(font)
        self.btnNum3.setObjectName("btnNum3")
        self.gridLayout.addWidget(self.btnNum3, 1, 2, 1, 1)
        self.btnNum1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum1.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum1.setFont(font)
        self.btnNum1.setObjectName("btnNum1")
        self.gridLayout.addWidget(self.btnNum1, 1, 0, 1, 1)
        self.btnNum5 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum5.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum5.setFont(font)
        self.btnNum5.setObjectName("btnNum5")
        self.gridLayout.addWidget(self.btnNum5, 3, 1, 1, 1)
        self.btnNum8 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum8.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum8.setFont(font)
        self.btnNum8.setObjectName("btnNum8")
        self.gridLayout.addWidget(self.btnNum8, 4, 1, 1, 1)
        self.btnNum6 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum6.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum6.setFont(font)
        self.btnNum6.setObjectName("btnNum6")
        self.gridLayout.addWidget(self.btnNum6, 3, 2, 1, 1)
        self.btnNum7 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum7.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum7.setFont(font)
        self.btnNum7.setObjectName("btnNum7")
        self.gridLayout.addWidget(self.btnNum7, 4, 0, 1, 1)
        self.btnNum4 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnNum4.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.btnNum4.setFont(font)
        self.btnNum4.setObjectName("btnNum4")
        self.gridLayout.addWidget(self.btnNum4, 3, 0, 1, 1)
        self.btnAccept = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnAccept.setEnabled(False)
        self.btnAccept.setMaximumSize(QtCore.QSize(16777215, 60))
        self.btnAccept.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.btnAccept.setStyleSheet("background-color: rgb(184, 255, 180);\n"
"font: 75 12pt \"MS Shell Dlg 2\";")
        self.btnAccept.setObjectName("btnAccept")
        self.gridLayout.addWidget(self.btnAccept, 5, 1, 1, 1)
        self.btnReject = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btnReject.setEnabled(False)
        self.btnReject.setMaximumSize(QtCore.QSize(16777215, 60))
        self.btnReject.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.btnReject.setStyleSheet("background-color: rgb(255, 167, 169);\n"
"font: 75 12pt \"MS Shell Dlg 2\";")
        self.btnReject.setObjectName("btnReject")
        self.gridLayout.addWidget(self.btnReject, 5, 2, 1, 1)
        self.lblPassword = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.lblPassword.setFont(font)
        self.lblPassword.setFrameShape(QtWidgets.QFrame.Panel)
        self.lblPassword.setText("")
        self.lblPassword.setAlignment(QtCore.Qt.AlignCenter)
        self.lblPassword.setObjectName("lblPassword")
        self.gridLayout.addWidget(self.lblPassword, 0, 0, 1, 3)

        self.retranslateUi(frmLock)
        QtCore.QMetaObject.connectSlotsByName(frmLock)
        frmLock.setTabOrder(self.btnNum1, self.btnNum4)
        frmLock.setTabOrder(self.btnNum4, self.btnNum5)
        frmLock.setTabOrder(self.btnNum5, self.btnNum6)
        frmLock.setTabOrder(self.btnNum6, self.btnNum7)
        frmLock.setTabOrder(self.btnNum7, self.btnNum8)
        frmLock.setTabOrder(self.btnNum8, self.btnNum9)
        frmLock.setTabOrder(self.btnNum9, self.btnNum0)

    def retranslateUi(self, frmLock):
        _translate = QtCore.QCoreApplication.translate
        frmLock.setWindowTitle(_translate("frmLock", "Introdusca contraseña"))
        self.btnNum2.setText(_translate("frmLock", "2"))
        self.btnNum9.setText(_translate("frmLock", "9"))
        self.btnNum0.setText(_translate("frmLock", "0"))
        self.btnNum3.setText(_translate("frmLock", "3"))
        self.btnNum1.setText(_translate("frmLock", "1"))
        self.btnNum5.setText(_translate("frmLock", "5"))
        self.btnNum8.setText(_translate("frmLock", "8"))
        self.btnNum6.setText(_translate("frmLock", "6"))
        self.btnNum7.setText(_translate("frmLock", "7"))
        self.btnNum4.setText(_translate("frmLock", "4"))
        self.btnAccept.setText(_translate("frmLock", "Save"))
        self.btnReject.setText(_translate("frmLock", "Cancel"))

